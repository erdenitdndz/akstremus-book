Инариус Мёртворождённый
-----------------------
Рождённый великой нежитью, Англаром, Инариус рыщет по миру в поисках знаний. Голоса в его голове, что не умолкают ни на миг, его единственные спутники по жизни.

**Заклинания**

* **1 круг**: 
    * `Нанесение ран <https://pf2e-ru-translation.readthedocs.io/ru/latest/spells/H/harm.html#spell-h-harm>`_
    * `Волшебное оружие <https://pf2e-ru-translation.readthedocs.io/ru/latest/spells/M/magic-weapon.html#spell-m-magic-weapon>`_
    * `Страх <https://pf2e-ru-translation.readthedocs.io/ru/latest/spells/F/fear.html#spell-f-fear>`_
    * `Защита <https://pf2e-ru-translation.readthedocs.io/ru/latest/spells/P/protection.html#spell-p-protection>`_
* **2 круг**:
* **3 круг**:
* **4 круг**: 

